import numpy as np
import os
import timeit
from typing import Union


def main() -> Union[str, int]:
    __location__: str = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))

    # Part 1
    with open(os.path.join(__location__, 'input.txt'), 'r') as raw_input:
        input_data: str = raw_input.read()

    input_array: np.array = np.array([np.sum(np.array(line, dtype=object)) for line in list(
        map(lambda each: list(map(int, each.split('\n'))), input_data.split('\n\n')))], dtype=object)

    max_idx: int = np.argmax(input_array)

    print(
        f'Elf number {max_idx} is carrying the most calories, with {input_array[max_idx]} calories.')

    # Part 2

    total_calories = np.sum(np.sort(input_array)[-3:])

    print(
        f'The three elves with the most calories, together, have {total_calories} calories.')


if __name__ == '__main__':
    start_time = timeit.default_timer()
    main()
    print(f'\nRan in: {timeit.default_timer() - start_time}s')