with open('input.txt') as f:
    lines = f.readlines()

d = {}
iter = 0

for line in lines:
    l = []
    d[iter] = d.get(iter, l)
    if line != "\n":
        d[iter].append(line)
    else:
        iter+=1
t = 0
for line, kcals in d.items():
    to_int = [int(cal.split("\n")[0]) for cal in kcals]
    total = sum(to_int)
    if (total > t) and (total < 67344):
        t = total

print(67658 + 67344 + 65156)