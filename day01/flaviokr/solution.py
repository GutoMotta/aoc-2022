with open("./input.txt") as f:
    inpt = f.read()

elves = inpt.split("\n\n")
calories_by_elf = []

for elf in elves:
    calories = elf.split("\n")
    calories = [int(cal) for cal in calories]
    calories_sum = sum(calories)

    calories_by_elf.append(calories_sum)

# Part 1
print(max(calories_by_elf))

# Part 2
print(sum(sorted(calories_by_elf)[-3:]))
