#!/bin/bash

# $1 is the path to the solution file.
# We cd into the solution directory to simplify relative file references.
runpy() (
  cd $(dirname $1)
  python $(basename $1)
  cd - >/dev/null
)