inputs = {"X": 1, "Y": 2, "Z": 3}

wins = {"A": "Y", "B": "Z", "C": "X"}
draw = {"A": "X", "B": "Y", "C": "Z"}
loses = {"A": "Z", "B": "X", "C": "Y"}

with open("input2.txt", "r") as f:
    lines = f.readlines()

points = 0
for line in lines:
    me = line[2].split("\n")[0]
    p2 = line[0]

    if me == "X":
        points += inputs[loses[p2]]
    elif me == "Y":
        points += inputs[draw[p2]]
        points += 3
    elif me == "Z":
        points += inputs[wins[p2]]
        points += 6

print(points)
