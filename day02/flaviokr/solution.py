with open("./input.txt") as f:
    inpt = f.read()

outcomes = inpt.split("\n")
outcomes = [outcome.split(" ") for outcome in outcomes]

# Part 1

translate = {
    "A": "rock",
    "B": "paper",
    "C": "scissors",
    "X": "rock",
    "Y": "paper",
    "Z": "scissors"
}

win_map = {
    "rock": "scissors",
    "paper": "rock",
    "scissors": "paper"
}

shapes_scores = {
    "rock": 1,
    "paper": 2,
    "scissors": 3
}

def calculate_score(myself, opponent):
    score = 0

    score += shapes_scores[myself]

    if myself == opponent:
        score += 3

    if win_map[myself] == opponent:
        score += 6

    return score

total_score = 0

for opponent, myself in outcomes:
    opponent = translate[opponent]
    myself = translate[myself]

    total_score += calculate_score(myself, opponent)

print(total_score)

# Part 2

translate = {
    "A": "rock",
    "B": "paper",
    "C": "scissors",
    "X": "lose",
    "Y": "draw",
    "Z": "win"
}

decision_map = {
    "win": {
        "rock": "paper",
        "paper": "scissors",
        "scissors": "rock"
    },
    "draw": {
        "rock": "rock",
        "paper": "paper",
        "scissors": "scissors"
    },
    "lose": {
        "rock": "scissors",
        "paper": "rock",
        "scissors": "paper"
    }
}

total_score = 0

for opponent, instruction in outcomes:
    opponent = translate[opponent]
    instruction = translate[instruction]

    myself = decision_map[instruction][opponent]

    total_score += calculate_score(myself, opponent)

print(total_score)
