with open("./input.txt") as f:
    inpt = f.read()

rucksacks = inpt.split("\n")

priorities = [
  "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
  "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
]

# Part 1

priorities_sum = 0

for items in rucksacks:
    half = int(len(items) / 2)

    first = items[:half]
    second = items[half:]

    item_in_both = list(set(first) & set(second))

    priorities_sum += priorities.index(*item_in_both) + 1 

print(priorities_sum)

# Part 2

priorities_sum = 0

while rucksacks:
    first = rucksacks.pop()
    second = rucksacks.pop()
    third = rucksacks.pop()

    item_in_all = list(set(first) & set(second) & set(third))

    priorities_sum += priorities.index(*item_in_all) + 1 

print(priorities_sum)
