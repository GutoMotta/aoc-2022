with open("input3.txt", "r") as f:
    sacks = f.readlines()

points = 0
for iter in range(0, len(sacks)-1, 3):
    group = sacks[iter:iter+3]

    common_char = list(set(group[0])&set(group[1])&set(group[2]))
    common_char.remove("\n")
    
    for char in common_char:
        if char.isupper():
            points += (ord(char) - 38)
        else:
            points += (ord(char) - 96)

print(points)