with open("input3.txt", "r") as f:
    sacks = f.readlines()

points = 0
for sack in sacks:
    compart_size = len(sack)//2
    c1 = sack[:compart_size]
    c2 = sack[compart_size:]

    common_char = list(set(c1)&set(c2))

    for char in common_char:
        if char.isupper():
            points += (ord(char) - 38)
        else:
            points += (ord(char) - 96)

print(points)