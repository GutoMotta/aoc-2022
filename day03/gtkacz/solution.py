import numpy as np
import os
import timeit
from functools import reduce


def main():
    # Part 1
    __location__: str = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))

    with open(os.path.join(__location__, 'input.txt'), 'r') as raw_input:
        raw_input_data: list = raw_input.read().splitlines()

    split_lines = np.array([(np.array(list(line[:len(line)//2]), dtype=object), np.array(list(line[len(line)//2:]), dtype=object))
                            for line in raw_input_data], dtype=object)

    def calculate_priority(input: str) -> int:
        if input.islower():
            return ord(input) - 96

        return ord(input) - 38

    sum_of_rucksacks = np.sum(np.array([calculate_priority(np.intersect1d(
        rucksack[0], rucksack[1])[0]) for rucksack in split_lines], dtype=object))

    print(
        f'The sum of the item type that appears in both compartments of each rucksack is {sum_of_rucksacks}.')

    # Part 2
    concat_lines = (np.array([reduce(np.intersect1d, (np.array(list(raw_input_data[line_idx]), dtype=object), np.array(list(raw_input_data[line_idx+1]),
                    dtype=object), np.array(list(raw_input_data[line_idx+2]), dtype=object))) for line_idx in range(len(raw_input_data))[::3]], dtype=object)).flatten()

    applyall = np.vectorize(calculate_priority)

    concat_lines_sum = np.sum(applyall(concat_lines))

    print(
        f'The sum of the item types that correspond to the badge of each of the three-Elf groups is {concat_lines_sum}.')


if __name__ == '__main__':
    start_time = timeit.default_timer()
    main()
    print(f'\nRan in: {timeit.default_timer() - start_time}s')
