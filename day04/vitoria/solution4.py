with open("input4.txt", "r") as f:
    chores = f.readlines()

points = 0
for sectors in chores:
    elf1 = list(map(int, sectors.split(",")[0].split("-")))
    elf2 = list(map(int, sectors.split(",")[1].split("-")))

    if elf1[0] >= elf2[0] and elf1[-1] <= elf2[-1]:
        points += 1
    elif elf2[0] >= elf1[0] and elf2[-1] <= elf1[-1]:
        points += 1

print(points)