with open("./input.txt") as f:
    inpt = f.read()

pairs = inpt.split("\n")

def parse_range(range_):
    return [int(num) for num in range_.split("-")]

# Part 1

def contains(range_a, range_b):
    a_start, a_end = parse_range(range_a)
    b_start, b_end = parse_range(range_b)

    return a_start <= b_start and a_end >= b_end

fully_contained_count = 0

for pair in pairs:
    first, second = pair.split(",")

    if contains(first, second) or contains(second, first):
        fully_contained_count +=1

print(fully_contained_count)

# Part 2

overlap_count = 0

def overlap(range_a, range_b):
    a_start, a_end = parse_range(range_a)
    b_start, b_end = parse_range(range_b)

    return (
        (a_start <= b_start and a_end >= b_start) or
        (a_start >= b_start and a_start <= b_end)
    )

for pair in pairs:
    first, second = pair.split(",")

    if overlap(first, second) or overlap(second, first):
        overlap_count +=1

print(overlap_count)