import numpy as np
import os
import timeit
from functools import reduce
from typing import Union


def main():
    __location__: str = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))

    # Part 1
    # Part 1
    with open(os.path.join(__location__, 'input.txt'), 'r') as raw_input:
        raw_input_data: np.array = np.array(
            raw_input.read().splitlines(), dtype=str)

    parsed_lines = np.array([np.array([range_str.split(
        '-') for range_str in line.split(',')]).flatten() for line in raw_input_data]).astype(np.int16)

    def range_is_contained(input: Union[list, np.array]) -> bool:
        if ((input[0] <= input[2]) and (input[1] >= input[3])) or ((input[0] >= input[2]) and (input[1] <= input[3])):
            return True

        return False

    result = np.count_nonzero(
        np.array(list(map(range_is_contained, parsed_lines)), dtype=bool))

    print(f'In {result} assignment pairs one range fully contains the other.')

    # Part 2
    def range_overlaps(input: Union[list, np.array]) -> bool:
        if ((input[0] <= input[2] and input[1] >= input[2]) or (input[0] <= input[3] and input[1] >= input[2])):
            return True

        return False

    result2 = np.count_nonzero(np.array(list(map(range_overlaps, parsed_lines)), dtype=bool))
    print(f'The ranges overlap in {result2} assignment pairs.')


if __name__ == '__main__':
    start_time = timeit.default_timer()
    main()
    print(f'\nRan in: {timeit.default_timer() - start_time}s')
